#!/bin/sh

if [ "$1" = "rust" ]; then
	7z a rust.zip server_rust_common/src server_rust_common/build.rs server_rust_common/Cargo.toml
	7z a rust.zip server_rust_mio/src server_rust_mio/Cargo.toml server_rust_mio/compile.sh
	7z a rust.zip README.md
elif [ "$1" = "kotlin" ]; then
	7z a kotlin.zip server_kotlin/src server_kotlin/build.gradle.kts server_kotlin/settings.gradle.kts
	7z a kotlin.zip README.md
else
	echo "usage: ./pack.sh rust|kotlin"
fi
