#!/usr/bin/env python3

import sys

import asyncio
import gzip
import struct

import esw_server_pb2

words = {}

def send_message(writer, message):
    data = message.SerializeToString()
    data_size = struct.pack("!I", len(data))
    writer.write(data_size + data)

def log_bin(name, data):
    print(
        f"{name}:",
        " ".join(
            map(
                lambda b: str(hex(b)),
                data
            )
        ),
        file = sys.stderr
    )

async def receive_message(reader, message_type):
    try:
        buffer = await reader.readexactly(4)
    except asyncio.IncompleteReadError as irerr:
        if len(irerr.partial) > 0:
            raise ValueError("Message length transmission incomplete")
        return None
    data_size, = struct.unpack("!I", buffer)
    try:
        data = await reader.readexactly(data_size)
    except asyncio.IncompleteReadError:
        raise ValueError("Data transmission incomplete")


    message = message_type()
    message.ParseFromString(data)

    # log_bin("buffer", buffer)
    # print("data_size:", data_size, file = sys.stderr)
    # log_bin("data", data)
    # print("message:", message, file = sys.stderr)
    return message


async def handle_connection(reader, writer):
    global words
    global loop

    # Client may ask multiple requests
    while True:
        request = await receive_message(reader, esw_server_pb2.Request)
        if request is None:
            break
        response = esw_server_pb2.Response()

        if request.HasField("getCount"):
            response.status = esw_server_pb2.Response.OK
            response.counter = len(words)
            send_message(writer, response)

            dump_words("input.log", words)
            words = {}
        elif request.HasField("postWords"):
            text = gzip.decompress(request.postWords.data).decode("utf-8")
            for word in text.split():
                if word not in words:
                    words[word] = 0
                words[word] += 1
            response.status = esw_server_pb2.Response.OK
            send_message(writer, response)
        else:
            raise Exception("Request broken")

def dump_words(path, words):
    with open(path, "w") as file:
        for (word, count) in words.items():
            print(
                " ".join(
                    [word for _ in range(count)]
                ),
                file = file
            )

loop = asyncio.get_event_loop()
coro = asyncio.start_server(handle_connection, '', 8080, loop=loop)
server = loop.run_until_complete(coro)

print('Serving on {}'.format(server.sockets[0].getsockname()))
loop.run_forever()