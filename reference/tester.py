#!/usr/bin/env python3

import time
import gzip
import struct
import socket
import sys
import os

import esw_server_pb2


class Client:
    def __init__(self, host, port):
        self.connection = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        self.connection.connect((host, port))
        print(f"Connected to {host}:{port}", file = sys.stderr)

    def recv(self, size):
        print(f"recv({size}) ", file = sys.stderr, end = "", flush = True)
        result = self.connection.recv(size)
        print(f"= {len(result)}", file = sys.stderr)

        return result

    def send_message(self, pb_message):
        data = pb_message.SerializeToString()
        data_size = struct.pack("!I", len(data))

        message = data_size + data
        print(f"send({len(message)}) ", file = sys.stderr, end = "", flush = True)
        result = self.connection.sendall(message)
        print(f"= {result}", file = sys.stderr)

    def receive_message(self, messageType):
        data_size, = struct.unpack("!I", self.recv(4))

        data = b''
        while data_size:
            buffer = self.recv(data_size)
            if not buffer:
                raise ValueError("Data transmission incomplete")
            data += buffer
            data_size -= len(buffer)
        message = messageType()
        message.ParseFromString(data)
        return message

    def get_count(self):
        request = esw_server_pb2.Request()
        request.getCount.CopyFrom(esw_server_pb2.Request.GetCount())
        self.send_message(request)
        response = self.receive_message(esw_server_pb2.Response)
        if response.status == esw_server_pb2.Response.OK:
            return response.counter
        else:
            raise ValueError("Status not OK: " + response.errMsg)

    def post_words(self, text):
        request = esw_server_pb2.Request()
        request.postWords.CopyFrom(esw_server_pb2.Request.PostWords())
        request.postWords.data = gzip.compress(text.encode("utf-8"))
        self.send_message(request)
        response = self.receive_message(esw_server_pb2.Response)
        if response.status != esw_server_pb2.Response.OK:
            raise ValueError("Status not OK: " + response.errMsg)


host = sys.argv[1] if len(sys.argv) > 1 else "127.0.0.1"
port = int(sys.argv[2]) if len(sys.argv) > 2 else 8080
client = Client(host, port)

before = time.perf_counter()

print("count(exp 0):", client.get_count())
client.post_words("two words")
client.post_words("three words words")
print("count(exp 3):", client.get_count())

ESW_TEST = os.environ["ESW_TEST"] if "ESW_TEST" in os.environ else "quick"

if ESW_TEST == "quick":
    print(">> Running quick test")
    with open("reference/quick.log", "r") as file:
        client.post_words(file.read())
        print("count(exp 91630):", client.get_count())
elif ESW_TEST == "static":
    print(">> Running static test")
    with open("reference/static.log", "r") as file:
        client.post_words(file.read())
        print("count(exp 358465):", client.get_count())

after = time.perf_counter()
elapsed = after - before
print("Elapsed:", elapsed)