use std::{
	fmt::Write,
	path::{Path, PathBuf}
};

const INCLUDES: [&'static str; 1] = ["../schema/"];
const SCHEMAS: [&'static str; 1] = ["../schema/esw_server.proto"];
const MODULES: [&'static str; 1] = ["esw_server"];

fn main() {
	for schema in SCHEMAS.iter() {
		println!("cargo:rerun-if-changed={}", schema);
	}

	let mut out_dir = PathBuf::from(std::env::var_os("OUT_DIR").unwrap());
	out_dir.push("proto");

	std::fs::create_dir(&out_dir).expect("Could not create out dir");

	let mut mod_path = out_dir.clone();
	mod_path.push("mod.rs");

	protobuf_codegen_pure::Codegen::new()
		.out_dir(&out_dir)
		.inputs(SCHEMAS.iter().map(|&s| Path::new(s)))
		.includes(INCLUDES.iter().map(|&s| Path::new(s)))
		.run()
		.expect("Codegen failed.");

	let mod_contents = MODULES.iter().fold(String::new(), |mut acc, module| {
		writeln!(&mut acc, "pub mod {};", module).unwrap();
		acc
	});
	std::fs::write(mod_path, mod_contents).expect("Could not write mod file");
}
