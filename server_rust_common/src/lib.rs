use std::{io::Read, sync::Arc};

use parking_lot::Mutex as ParkMutex;
pub use parking_lot::Condvar;
use set_trait::SetTrait;

#[macro_use]
pub mod macros;
pub mod metrics;
pub mod proto;
pub mod set_trait;

// pub mod shared_sets;

named_metric!(EndEpoch);
named_metric!(ParseRequest);
named_metric!(Decompress);
named_metric!(Count);
named_metric!(PerformWork);
named_metric!(SynchronizeNow);

pub type WordSet = dashmap::DashSet<&'static [u8], metrohash::MetroBuildHasher>;
pub type LocalWordSet = std::collections::HashSet<&'static [u8], metrohash::MetroBuildHasher>;

pub const MESSAGE_SIZE_SIZE: usize = 4;
pub const COUNT_MESSAGE_SIZE: usize = 2;
pub type Mutex<T> = ParkMutex<T>;


struct LocalSetsHandlerInner {
	shared_set: WordSet,
	shared_leaks: Mutex<Vec<Vec<u8>>>
}
pub struct LocalSetsHandler {
	shared: Arc<LocalSetsHandlerInner>
}
impl LocalSetsHandler {
	pub fn new() -> Self {
		let shared = Arc::new(
			LocalSetsHandlerInner {
				shared_set: WordSet::default(),
				shared_leaks: Mutex::new(Vec::new())
			}
		);
		
		LocalSetsHandler {
			shared
		}
	}

	pub fn new_worker_context(&self) -> LocalSetsWorker {
		LocalSetsWorker {
			shared: self.shared.clone(),
			local_set: LocalWordSet::default(),
			
			metrics_parse_request: Default::default(),
			metrics_decompress: Default::default(),
			metrics_count: Default::default(),
			metrics_perform_work: Default::default(),
			metrics_synchronize_now: Default::default(),
			metrics_end_epoch: Default::default(),
		}
	}
}

pub struct LocalSetsWorker {
	shared: Arc<LocalSetsHandlerInner>,
	local_set: LocalWordSet,

	pub metrics_parse_request: ParseRequest::Metric,
	pub metrics_decompress: Decompress::Metric,
	pub metrics_count: Count::Metric,
	pub metrics_perform_work: PerformWork::Metric,
	pub metrics_synchronize_now: SynchronizeNow::Metric,
	pub metrics_end_epoch: EndEpoch::Metric
}
impl LocalSetsWorker {
	pub fn synchronize_now(&mut self) {
		self.metrics_synchronize_now.enter();
		for word in self.local_set.drain() {
			self.shared.shared_set.insert(word);
		}
		self.metrics_synchronize_now.leave();
	}

	pub fn perform_work(&mut self, data: &[u8]) -> protobuf::ProtobufResult<()> {
		self.metrics_perform_work.enter();

		let mut leak_buffer = Vec::new();
		unsafe {
			perform_work(
				data,
				&mut self.local_set,
				&mut leak_buffer,

				&mut self.metrics_parse_request,
				&mut self.metrics_decompress,
				&mut self.metrics_count
			)?
		};

		// update shared state
		{
			let mut lock = self.shared.shared_leaks.lock();
			lock.push(leak_buffer);
		}

		self.metrics_perform_work.leave();
		Ok(())
	}

	pub fn end_epoch(&mut self) -> i32 {
		self.metrics_end_epoch.enter();

		self.shared.shared_set.insert(&[]);
		let count = self.shared.shared_set.len() - 1;
		self.shared.shared_set.clear();

		{
			let mut lock = self.shared.shared_leaks.lock();
			lock.clear();
		}

		self.metrics_end_epoch.leave();
		count as i32
	}
}

fn decompress(
	compressed_data: &[u8],
	out: &mut Vec<u8>
) {
	let mut decoder = flate2::bufread::DeflateDecoder::new(
		&compressed_data[10..]
	);

	decoder.read_to_end(out).unwrap();
}

/// ### Safety
/// * The caller must ensure that the data deposited into `leak_buffer` outlives the references in `set`.
unsafe fn perform_work(
	data: &[u8],
	mut set: impl SetTrait<&'static [u8]>,
	leak_buffer: &mut Vec<u8>,

	metrics_parse_request: &mut ParseRequest::Metric,
	metrics_decompress: &mut Decompress::Metric,
	metrics_count: &mut Count::Metric,
) -> protobuf::ProtobufResult<()> {
	// parse the message
	metrics_parse_request.enter();
	let compressed_data = {
		let mut input_stream = protobuf::CodedInputStream::from_bytes(data);

		// Request tag
		let tag = input_stream.read_tag_unpack()?;
		debug_assert_eq!(tag, (2, protobuf::wire_format::WireTypeLengthDelimited));

		// ??
		input_stream.read_raw_varint64()?;

		// PostWords tag
		let tag = input_stream.read_tag_unpack()?;
		debug_assert_eq!(tag, (1, protobuf::wire_format::WireTypeLengthDelimited));

		// data len
		let len = input_stream.read_raw_varint32()? as usize;
		let start = input_stream.pos() as usize;

		&data[start .. start + len]
	};
	metrics_parse_request.leave();

	// decompress the stream
	metrics_decompress.enter();
	decompress(compressed_data, leak_buffer);
	metrics_decompress.leave();

	// count the words locally
	metrics_count.enter();
	for word in leak_buffer.split(u8::is_ascii_whitespace) {
		#[allow(unused_unsafe)]
		let leaked_word: &'static [u8] = unsafe {
			std::slice::from_raw_parts(word.as_ptr(), word.len())
		};

		set.insert(leaked_word);
	}
	metrics_count.leave();

	Ok(())
}

pub fn parse_args() -> (String, u16) {
	const ADDRESS: (&'static str, u16) = ("127.0.0.1", 8080);
	
	let address = {
		let host = match std::env::var("ESW_HOST") {
			Err(_) => ADDRESS.0.to_string(),
			Ok(host) => host
		};

		let port = match std::env::var("ESW_PORT") {
			Err(_) => ADDRESS.1,
			Ok(port) => {
				let port: u16 = port.parse().unwrap();
				port
			}
		};

		(host, port)
	};

	address
}