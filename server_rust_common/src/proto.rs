include!(concat!(env!("OUT_DIR"), "/proto/mod.rs"));

use protobuf::Message;

const BUFFER_SIZE: usize = 16;

pub struct Responder {
	buffer: [u8; BUFFER_SIZE],
	current_size: usize
}
impl Responder {
	pub const fn new() -> Self {
		Responder {
			buffer: [0u8; BUFFER_SIZE],
			current_size: 0
		}
	}

	pub fn buffer(&self) -> &[u8] {
		&self.buffer[.. self.current_size]
	}

	pub fn post_words_response(&mut self) {
		let response = esw_server::Response::new();

		// write the size
		let size = response.compute_size();
		log!("words[{}]", size);
		let size_be = size.to_be_bytes();
		self.buffer[0] = size_be[0];
		self.buffer[1] = size_be[1];
		self.buffer[2] = size_be[2];
		self.buffer[3] = size_be[3];

		let mut output_stream = protobuf::CodedOutputStream::bytes(&mut self.buffer[4..]);
		response.write_to_with_cached_sizes(&mut output_stream).unwrap();
		
		self.current_size = 4 + size as usize;
	}

	pub fn get_count_response(&mut self, counter: i32) {
		let mut response = esw_server::Response::new();
		response.counter = counter;

		// write the size
		let size = response.compute_size();
		log!("count[{}]: {:?}", size, response);
		let size_be = size.to_be_bytes();
		self.buffer[0] = size_be[0];
		self.buffer[1] = size_be[1];
		self.buffer[2] = size_be[2];
		self.buffer[3] = size_be[3];
		
		let mut output_stream = protobuf::CodedOutputStream::bytes(&mut self.buffer[4..]);
		response.write_to_with_cached_sizes(&mut output_stream).unwrap();
		
		self.current_size = 4 + size as usize;
	}
}