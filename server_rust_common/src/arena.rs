use std::{mem::MaybeUninit, sync::atomic::{AtomicUsize, Ordering}};

pub struct ArenaChunk {
	chunk: *mut u8,
	len: usize
}
impl ArenaChunk {
	pub fn write(&mut self, data: &[u8]) {
		assert_eq!(data.len(), self.len);

		unsafe {
			std::ptr::copy_nonoverlapping(
				data.as_ptr(),
				self.chunk,
				self.len
			)
		}
	}
	
	pub unsafe fn assume_init(self) -> &'static mut [u8] {
		std::slice::from_raw_parts_mut(
			self.chunk,
			self.len
		)
	}
}

/// A global arena of fixed amount of memory that can be claimed from any thread.
pub struct GlobalArena {
	memory: Box<[MaybeUninit<u8>]>,
	head: AtomicUsize
}
impl GlobalArena {
	pub fn new(capacity: usize) -> Self {
		let memory = unsafe {
			let layout = std::alloc::Layout::from_size_align(
				capacity,
				std::mem::align_of::<u8>()
			).unwrap();

			let raw_memory = std::alloc::alloc(layout) as *mut MaybeUninit<u8>;

			let raw_memory_slice = std::ptr::slice_from_raw_parts_mut(
				raw_memory,
				capacity
			);

			Box::from_raw(raw_memory_slice)
		};
		
		GlobalArena {
			memory,
			head: AtomicUsize::new(0)
		}
	}

	pub fn borrow(&self, len: usize) -> ArenaChunk {
		let start = self.head.fetch_add(len, Ordering::Relaxed);
		if start + len >= self.memory.len() {
			panic!("Arena out of memory")
		}

		// SAFETY: range start .. start + len is contained by self.memory
		let chunk = unsafe { self.memory.as_ptr().add(start) as *mut u8 };

		ArenaChunk {
			chunk,
			len
		}
	}

	/// ### Safety
	/// * There must be no outstanding borrows, otherwise their memory may be mutated by a future call to `borrow`.
	pub unsafe fn reset(&self) {
		self.head.store(0, Ordering::Relaxed)
	}
}