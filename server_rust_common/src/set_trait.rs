use std::{collections::HashSet, hash::{BuildHasher, Hash}};

pub trait SetTrait<T> {
	fn contains(&self, key: &T) -> bool;
	fn insert(&mut self, key: T) -> bool;
}

impl<T: Eq + Hash, H: BuildHasher> SetTrait<T> for HashSet<T, H> {
	fn contains(&self, key: &T) -> bool {
		HashSet::<T, H>::contains(self, key)
	}

	fn insert(&mut self, key: T) -> bool {
		HashSet::<T, H>::insert(self, key)
	}
}

impl<T: Eq + Hash, H: BuildHasher + Clone> SetTrait<T> for dashmap::DashSet<T, H> {
	fn contains(&self, key: &T) -> bool {
		dashmap::DashSet::<T, H>::contains(self, key)
	}

	fn insert(&mut self, key: T) -> bool {
		dashmap::DashSet::<T, H>::insert(self, key)
	}
}

impl<T: Eq + Hash, S: SetTrait<T>> SetTrait<T> for &mut S {
	fn contains(&self, key: &T) -> bool {
		(**self).contains(key)
	}

	fn insert(&mut self, key: T) -> bool {
		(**self).insert(key)
	}
}