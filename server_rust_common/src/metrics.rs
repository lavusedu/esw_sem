#[macro_export]
macro_rules! named_metric {
	(
		$name: ident
	) => {
		#[allow(non_snake_case)]
		mod $name {
			#[derive(Copy, Clone)]
			pub struct Name;
			impl $crate::metrics::MetricName for Name {
				const NAME: &'static str = stringify!($name);
			}
			pub type Metric = $crate::metrics::Metrics<Name>;
		}
	};
}

pub trait MetricName {
	const NAME: &'static str;
}
impl MetricName for () {
	const NAME: &'static str = "";
}

#[cfg(feature = "metrics")]
mod inner {
	use super::MetricName;

	#[derive(Clone)]
	pub struct Metrics<Name: MetricName = ()> {
		pub hit_count: u32,
		pub total_time: f64,
		pub last_start: std::time::Instant,
		pub _name: std::marker::PhantomData<Name>
	}
	impl<Name: MetricName> Metrics<Name> {
		pub fn new() -> Self {
			Metrics {
				hit_count: 0,
				total_time: 0.0,
				last_start: std::time::Instant::now(),
				_name: std::marker::PhantomData
			}
		}

		pub fn enter(&mut self) {
			self.hit();
			self.start();
		}

		pub fn leave(&mut self) {
			self.stop();
		}

		fn hit(&mut self) {
			self.hit_count += 1;
		}

		fn start(&mut self)  {
			self.last_start = std::time::Instant::now();
		}

		fn stop(&mut self) {
			let now = std::time::Instant::now();
			let elapsed = now.duration_since(self.last_start);

			self.total_time += elapsed.as_secs_f64();
		}
	}
	impl<Name: MetricName> std::fmt::Debug for Metrics<Name> {
		fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
			write!(
				f,
				"Metrics<{}>[{:p}] {{ hit_count: {}, total_time: {} }}",
				Name::NAME,
				self as *const _,
				self.hit_count, self.total_time
			)
		}
	}
	impl<Name: MetricName> Default for Metrics<Name> {
		fn default() -> Self {
			Self::new()
		}
	}
	impl<Name: MetricName> Drop for Metrics<Name> {
		fn drop(&mut self) {
			eprintln!("{:?}", self);
		}
	}
}

#[cfg(not(feature = "metrics"))]
mod inner {
	use super::MetricName;

	#[derive(Clone)]
	pub struct Metrics<Name: MetricName = ()>(std::marker::PhantomData<Name>);
	impl<Name: MetricName> Metrics<Name> {
		pub fn new() -> Self {
			Metrics(std::marker::PhantomData)
		}

		pub fn enter(&mut self) {}

		pub fn leave(&mut self) {}
	}
	impl<Name: MetricName> std::fmt::Debug for Metrics<Name> {
		fn fmt(&self, _: &mut std::fmt::Formatter) -> std::fmt::Result {
			Ok(())
		}
	}
	impl<Name: MetricName> Default for Metrics<Name> {
		fn default() -> Self {
			Self::new()
		}
	}
	impl<Name: MetricName> Drop for Metrics<Name> {
		fn drop(&mut self) {}
	}
}

pub use inner::*;