use std::io::{self, Read};

pub trait ReadSplitTrait {
	fn scan_read(&mut self) -> io::Result<Option<&'_ [u8]>>;
}

pub struct ReadSplit<R: Read, P: Fn(u8) -> bool, const N: usize> {
	source: R,
	buffer: [u8; N],
	// Start of the current match
	start: usize,
	/// Position of the current match
	position: usize,
	/// Size of the data read into the buffer.
	data_size: usize,
	predicate: P
}
impl<R: Read, P: Fn(u8) -> bool, const N: usize> ReadSplit<R, P, N> {
	pub fn new(source: R, predicate: P) -> Self {
		ReadSplit {
			source,
			buffer: [0u8; N],
			start: 0,
			position: 0,
			data_size: 0,
			predicate
		}
	}
	
	fn scan(&mut self) -> Option<(usize, usize)> {
		// scan existing data
		while self.position < self.data_size {
			let byte = self.buffer[self.position];

			if (self.predicate)(byte) {
				let value = (self.start, self.position);
				
				self.position += 1;
				self.start = self.position;
				return Some(value)
			}

			self.position += 1;
		}

		None
	}

	pub fn scan_read(&mut self) -> io::Result<Option<&'_ [u8]>> {
		loop {
			match self.scan() {
				Some((start, end)) => return Ok(Some(&mut self.buffer[start .. end])),
				None => ()
			};

			// scanned up to the end of currently read data
			// need to read more

			// shift the content to the left if the hit the end of the buffer (sort of)
			if self.position >= self.buffer.len() - self.buffer.len() / 5 {
				if self.start == 0 {
					panic!("set the buffer size higher so that it fits more than one word");
				}

				let remaining_size = self.data_size - self.start;
				if remaining_size <= self.start {
					// go non-overlapping when we can
					unsafe {
						std::ptr::copy_nonoverlapping(
							self.buffer[self.start..].as_ptr(),
							self.buffer[0..].as_mut_ptr(),
							remaining_size
						);
					}
				} else {
					unsafe {
						std::ptr::copy(
							self.buffer[self.start..].as_ptr(),
							self.buffer[0..].as_mut_ptr(),
							remaining_size
						);
					}
				}

				self.data_size = remaining_size;
				self.position = remaining_size;
				self.start = 0;
			}

			match self.source.read(&mut self.buffer[self.position ..]) {
				Ok(0) => return Ok(None), // stream is done
				Ok(n) => { self.data_size += n; },
				Err(err) => return Err(err)
			}
		}
	}
}
impl<R: Read, P: Fn(u8) -> bool, const N: usize> ReadSplitTrait for ReadSplit<R, P, N> {
	fn scan_read(&mut self) -> io::Result<Option<&'_ [u8]>> {
		self.scan_read()
	}
}

#[cfg(test)]
mod test {
	use super::ReadSplit;

	#[test]
	fn test_stream_split_hasher() {
		let source = [1u8, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3];
		let mut read_split: ReadSplit<_, _, 5> = ReadSplit::new(
			AsRef::<[u8]>::as_ref(&source),
			|b| b == 3
		);

		assert_eq!(
			read_split.scan_read().unwrap(), Some([1, 2].as_ref())
		);
		assert_eq!(
			read_split.scan_read().unwrap(), Some([1, 2].as_ref())
		);
		assert_eq!(
			read_split.scan_read().unwrap(), Some([1, 2].as_ref())
		);
		assert_eq!(
			read_split.scan_read().unwrap(), Some([1, 2].as_ref())
		);
		assert_eq!(
			read_split.scan_read().unwrap(), None
		);
	}
}