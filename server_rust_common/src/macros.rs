#[macro_export]
macro_rules! log {
	(
		$($arg: tt)*
	) => {
		if cfg!(debug_assertions) {
			eprintln!(
				"[{:?}] {}",
				std::thread::current().id(),
				format_args!($($arg)*)
			);
		}
	}
}