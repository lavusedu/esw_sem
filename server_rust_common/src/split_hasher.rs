use std::{io::Read, hash::Hasher};

pub struct SplitHasher<R: Read, P: Fn(u8) -> bool, H: Hasher + Default, const N: usize> {
	source: R,
	buffer: [u8; N],
	position: usize,
	size: usize,
	hasher: H,
	predicate: P
}
impl<R: Read, P: Fn(u8) -> bool, H: Hasher + Default, const N: usize> SplitHasher<R, P, H, N> {
	pub fn new(source: R, predicate: P) -> Self {
		SplitHasher {
			source,
			buffer: [0u8; N],
			position: 0,
			size: 0,
			hasher: H::default(),
			predicate
		}
	}
	
	fn scan(&mut self) -> Option<u64> {
		let start = self.position;

		while self.position < self.size {
			let byte = self.buffer[self.position];
			self.position += 1;

			if (self.predicate)(byte) {
				self.hasher.write(
					&self.buffer[start .. self.position - 1]
				);
				let value = self.hasher.finish();
				
				self.hasher = H::default();

				return Some(value)
			}
		}

		self.hasher.write(
			&self.buffer[start .. self.position]
		);

		None
	}

	fn scan_read(&mut self) -> Option<u64> {
		let mut scan_result = self.scan();
		while scan_result.is_none() {
			let read_size = match self.source.read(&mut self.buffer) {
				Ok(0) => return None,
				Ok(n) => n,
				Err(err) => panic!("{}", err)
			};

			self.size = read_size;
			self.position = 0;
			scan_result = self.scan();
		}

		scan_result
	}
}
impl<R: Read, P: Fn(u8) -> bool, H: Hasher + Default, const N: usize> Iterator for SplitHasher<R, P, H, N> {
	type Item = u64;
	
	fn next(&mut self) -> Option<Self::Item> {
		self.scan_read()
	}
}

#[cfg(test)]
mod test {
	use std::{
		hash::Hasher,
		collections::hash_map::DefaultHasher
	};
	use super::SplitHasher;

	#[test]
	fn test_stream_split_hasher() {
		let source = [1u8, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3];
		let split_hasher: SplitHasher<_, _, DefaultHasher, 5> = SplitHasher::new(
			AsRef::<[u8]>::as_ref(&source),
			|b| b == 3
		);

		let reference = {
			let mut hasher = DefaultHasher::default();
			hasher.write(&[1, 2]);
			hasher.finish()
		};

		for value in split_hasher {
			assert_eq!(value, reference);
		}
	}
}