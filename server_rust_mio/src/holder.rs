use std::{mem::ManuallyDrop, sync::atomic::{AtomicU32, Ordering}};

// #[repr(align(128))]
pub struct TaskHolder<T> {
	pub flag: AtomicU32,
	data: ManuallyDrop<T>
}
impl<T> TaskHolder<T> {
	const VERSION_MASK: u32 = 0xFFFFFF_00;
	const VERSION_BUMP: u32 = 0x000001_00;
	const STATE_MASK: u32 = 0x000000_0F;

	const STATE_FREE: u32 = 0;
	const STATE_QUEUED: u32 = 1;
	const STATE_INPROGRESS: u32 = 2;
	const STATE_DEAD: u32 = 4;

	pub fn new(data: T) -> Self {
		TaskHolder {
			flag: AtomicU32::new(0),
			data: ManuallyDrop::new(data)
		}
	} 

	pub fn new_leak(data: T) -> &'static mut Self {
		let me = Box::new(
			Self::new(data)
		);
		
		Box::leak(me)
	}

	/// ### Safety
	/// * Token must the reference from `Self::new_leak` cast to usize.
	pub unsafe fn from_token(token: usize) -> &'static mut Self {
		&mut *(token as *mut Self)
	}

	/// ### Safety
	/// * State must not be dead
	pub unsafe fn data(&mut self) -> &mut T {
		// debug_assert_ne!(self.flag.load(Ordering::Relaxed) & Self::STATE_MASK, Self::STATE_DEAD);
		
		&mut self.data
	}

	/// Checks the state and decides what do to when the intent is to enqueue:
	/// * Returns `true` if the state is free
	/// * Returns `false` and does nothing if the state is queued
	/// * Returns `false` and updates the version if the state is in progress
	/// * Returns `false` and does nothing if the state is dead
	pub fn update_enqueue(&mut self) -> bool {
		let flag = self.flag.load(Ordering::Relaxed);
		
		let state = flag & Self::STATE_MASK;
		match state {
			Self::STATE_FREE => {
				self.flag.store(Self::STATE_QUEUED, Ordering::Relaxed);
				true
			}
			Self::STATE_QUEUED => false,
			Self::STATE_INPROGRESS => {
				// attempt to bump the version
				match self.flag.compare_exchange_weak(
					flag, flag + Self::VERSION_BUMP,
					Ordering::Relaxed, Ordering::Relaxed
				) {
					Ok(_) => {
						server_rust_common::log!("Bumped version to {}", (flag + Self::VERSION_BUMP) & Self::VERSION_MASK);
						false
					}
					Err(_) => {
						debug_assert_eq!(
							self.flag.load(Ordering::Relaxed) & Self::STATE_MASK,
							Self::STATE_FREE
						);

						// if failed, the state must be free already
						self.flag.store(Self::STATE_QUEUED, Ordering::Relaxed);
						true
					}
				}
			},
			Self::STATE_DEAD => false,
			_ => {
				#[cfg(debug_assertions)]
				unreachable!();

				#[cfg_attr(debug_assertions, allow(unreachable_code))]
				unsafe { std::hint::unreachable_unchecked(); }
			}
		}
	}

	/// Updates the flag when the value is dequeued.
	///
	/// Panics if the value is not enqueued.
	///
	/// Returns the current version.
	pub fn update_dequeue(&mut self) -> u32 {
		let flag = self.flag.compare_exchange_weak(
			Self::STATE_QUEUED, Self::STATE_INPROGRESS,
			Ordering::AcqRel, Ordering::Relaxed
		).unwrap();

		flag & Self::VERSION_MASK
	}

	/// Updates the holder and drops the value when the value is dead.
	///
	/// ### Safety
	/// * State must be in progress or free
	pub unsafe fn update_dead(&mut self) {
		debug_assert!(
			{
				let state = self.flag.load(Ordering::Relaxed) & Self::STATE_MASK;
				state == Self::STATE_INPROGRESS || state == Self::STATE_FREE
			}
		);

		self.flag.store(Self::STATE_DEAD, Ordering::Release);
		ManuallyDrop::drop(&mut self.data);
	}

	/// Updates the flag when the intent is to go from inprogress to free:
	/// * Returns `Ok(())` if the operation succeeded.
	/// * Returns `Err(new_version)` if the operation failed because a new version has been added.
	///
	/// ### Safety
	/// * State must be in progress
	pub unsafe fn update_free(&mut self, version: u32) -> Result<(), u32> {
		match self.flag.compare_exchange_weak(
			Self::STATE_INPROGRESS + version, Self::STATE_FREE,
			Ordering::AcqRel, Ordering::Relaxed
		) {
			Ok(_) => Ok(()),
			Err(new_value) => {
				debug_assert_eq!(new_value & Self::STATE_MASK, Self::STATE_INPROGRESS);

				server_rust_common::log!("Detected version bump from {} to {}", version, new_value & Self::VERSION_MASK);
				Err(new_value & Self::VERSION_MASK)
			}
		}
	}

	pub fn is_free(&mut self) -> bool {
		self.flag.load(Ordering::Relaxed) & Self::STATE_MASK == Self::STATE_FREE
	}

	pub fn is_dead(&mut self) -> bool {
		self.flag.load(Ordering::Relaxed) & Self::STATE_MASK == Self::STATE_DEAD
	}
}

#[cfg(test)]
mod test {
	use std::{mem::ManuallyDrop, sync::atomic::{AtomicU32, Ordering}};

	type TaskHolder = super::TaskHolder<usize>;

	#[test]
	fn test_task_holder_enqueue() {
		let mut holder = TaskHolder {
			flag: AtomicU32::new(TaskHolder::STATE_FREE),
			data: ManuallyDrop::new(0usize)
		};
		assert_eq!(holder.update_enqueue(), true);
		assert_eq!(holder.flag.load(Ordering::Relaxed), TaskHolder::STATE_QUEUED);
		assert_eq!(holder.update_enqueue(), false);

		let mut holder = TaskHolder {
			flag: AtomicU32::new(TaskHolder::STATE_INPROGRESS),
			data: ManuallyDrop::new(0usize)
		};
		assert_eq!(holder.update_enqueue(), false);
		assert_eq!(holder.flag.load(Ordering::Relaxed), TaskHolder::STATE_INPROGRESS + TaskHolder::VERSION_BUMP);
		assert_eq!(holder.update_enqueue(), false);
		assert_eq!(holder.flag.load(Ordering::Relaxed), TaskHolder::STATE_INPROGRESS + TaskHolder::VERSION_BUMP * 2);

		let mut holder = TaskHolder {
			flag: AtomicU32::new(TaskHolder::STATE_DEAD),
			data: ManuallyDrop::new(0usize)
		};
		assert_eq!(holder.update_enqueue(), false);
		assert_eq!(holder.flag.load(Ordering::Relaxed), TaskHolder::STATE_DEAD);
	}

	#[test]
	fn test_task_holder_dequeue() {
		let mut holder = TaskHolder {
			flag: AtomicU32::new(TaskHolder::STATE_QUEUED),
			data: ManuallyDrop::new(0usize)
		};
		assert_eq!(holder.update_dequeue(), 0);
		assert_eq!(holder.flag.load(Ordering::Relaxed), TaskHolder::STATE_INPROGRESS);
	}

	#[test]
	#[should_panic]
	fn test_task_holder_dequeue_free() {
		let mut holder = TaskHolder {
			flag: AtomicU32::new(TaskHolder::STATE_FREE),
			data: ManuallyDrop::new(0usize)
		};
		holder.update_dequeue();
	}

	#[test]
	#[should_panic]
	fn test_task_holder_dequeue_inprogress() {
		let mut holder = TaskHolder {
			flag: AtomicU32::new(TaskHolder::STATE_INPROGRESS),
			data: ManuallyDrop::new(0usize)
		};
		holder.update_dequeue();
	}

	#[test]
	#[should_panic]
	fn test_task_holder_dequeue_dead() {
		let mut holder = TaskHolder {
			flag: AtomicU32::new(TaskHolder::STATE_DEAD),
			data: ManuallyDrop::new(0usize)
		};
		holder.update_dequeue();
	}

	#[test]
	fn test_task_holder_dead() {
		let mut holder = TaskHolder {
			flag: AtomicU32::new(TaskHolder::STATE_INPROGRESS),
			data: ManuallyDrop::new(0usize)
		};
		unsafe { holder.update_dead() };
		assert_eq!(holder.flag.load(Ordering::Relaxed), TaskHolder::STATE_DEAD);
	}

	#[test]
	fn test_task_holder_free() {
		let mut holder = TaskHolder {
			flag: AtomicU32::new(TaskHolder::STATE_INPROGRESS),
			data: ManuallyDrop::new(0usize)
		};
		assert_eq!(unsafe { holder.update_free(0) }, Ok(()));
		assert_eq!(holder.flag.load(Ordering::Relaxed), TaskHolder::STATE_FREE);

		let mut holder = TaskHolder {
			flag: AtomicU32::new(TaskHolder::STATE_INPROGRESS + TaskHolder::VERSION_BUMP),
			data: ManuallyDrop::new(0usize)
		};
		assert_eq!(unsafe { holder.update_free(0) }, Err(TaskHolder::VERSION_BUMP));
		assert_eq!(holder.flag.load(Ordering::Relaxed), TaskHolder::STATE_INPROGRESS + TaskHolder::VERSION_BUMP);

		assert_eq!(unsafe { holder.update_free(TaskHolder::VERSION_BUMP) }, Ok(()));
		assert_eq!(holder.flag.load(Ordering::Relaxed), TaskHolder::STATE_FREE);
	}
}