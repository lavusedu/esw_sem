use std::{net::ToSocketAddrs, sync::Arc};

use holder::TaskHolder;
use mio::{
	net::TcpListener,
	Events, Interest, Poll, Token
};

use server_rust_common::{Condvar, LocalSetsHandler, LocalSetsWorker, Mutex, named_metric};

pub mod task;
pub mod queue;
pub mod holder;

use queue::{FixedQueue, FixedQueueConsumer};
use task::{CountTask, CountTaskResult};

const SERVER_TOKEN: Token = Token(0);
#[cfg(feature = "metrics")]
const STDIN_TOKEN: Token = Token(1);
const THREAD_COUNT: usize = 15;
const QUEUE_SIZE: usize = 1024;

type TaskQueue = FixedQueue<usize, QUEUE_SIZE>;
type TaskQueueConsumer = FixedQueueConsumer<usize, QUEUE_SIZE>;
type CountTaskHolder = TaskHolder<CountTask>;

struct WorkLockData {
	pub barrier_in_progress: bool,
	pub shutdown_in_progress: bool,
	pub waiting: usize
}
struct WorkerSync {
	work_lock: Mutex<WorkLockData>,
	work_cond: Condvar,
	barrier_leader_cond: Condvar,
	barrier_cond: Condvar
}
impl WorkerSync {
	pub fn new() -> Arc<Self> {
		let me = WorkerSync {
			work_lock: Mutex::new(
				WorkLockData {
					barrier_in_progress: false,
					shutdown_in_progress: false,
					waiting: 0
				}
			),
			work_cond: Condvar::new(),
			barrier_leader_cond: Condvar::new(),
			barrier_cond: Condvar::new()
		};

		Arc::new(me)
	}

	/// Waits for work, respecting an ongoing barrier.
	///
	/// Returns true if shutdown is happening.
	pub fn wait_for_work(&self) -> bool {
		let mut lock = self.work_lock.lock();

		if lock.barrier_in_progress {
			server_rust_common::log!("Notifying barrier");
			
			lock.waiting += 1;
			self.barrier_leader_cond.notify_one();
			self.barrier_cond.wait(&mut lock);
			lock.waiting -= 1;

			server_rust_common::log!("Barrier end");
		}
		#[cfg(feature = "metrics")]
		if lock.shutdown_in_progress {
			return true;
		}

		server_rust_common::log!("Waiting for work");
		// occasionally wake up and spin
		self.work_cond.wait_for(&mut lock, std::time::Duration::from_millis(100));

		server_rust_common::log!("Queue cond wakeup");
		false
	}

	/// Issues a barrier wait and blocks until the barrier is hit.
	///
	/// If another barrier is already in progress, waits on that barrier instead.
	pub fn issue_barrier(&self) {
		let mut lock = self.work_lock.lock();
		if lock.barrier_in_progress {
			server_rust_common::log!("Merging barrier");
			
			lock.waiting += 1;
			self.barrier_leader_cond.notify_one();
			self.barrier_cond.wait(&mut lock);
			lock.waiting -= 1;

			server_rust_common::log!("Merged barrier end");
		} else {
			// new barrier
			lock.barrier_in_progress = true;

			server_rust_common::log!("Issuing barrier");
			self.work_cond.notify_all();
			while lock.waiting < THREAD_COUNT - 1 {
				self.barrier_leader_cond.wait(&mut lock);
			}
			self.barrier_cond.notify_all();
			
			lock.barrier_in_progress = false;
			server_rust_common::log!("Barrier leader end");
		}
	}

	pub fn issue_shutdown(&self) {
		let mut lock = self.work_lock.lock();
		server_rust_common::log!("Issuing shutdown");
		lock.shutdown_in_progress = true;
		self.work_cond.notify_all();
	}
}

fn worker_thread_run(
	mut queue: TaskQueueConsumer,
	mut context: LocalSetsWorker,
	sync: Arc<WorkerSync>
) -> std::io::Result<()> {
	named_metric!(WorkerThreadRunTask);
	let mut metric_run_task = WorkerThreadRunTask::Metric::new();
	named_metric!(WorkerThreadFindWork);
	let mut metric_find_work = WorkerThreadFindWork::Metric::new();
	named_metric!(WorkerThreadFindWorkBlock);
	let mut metric_find_work_block = WorkerThreadFindWorkBlock::Metric::new();
	named_metric!(WorkerThreadIssueBarrier);
	let mut metric_issue_barrier = WorkerThreadIssueBarrier::Metric::new();
	named_metric!(WorkerThreadSynchronizeNow);
	let mut metric_synchronize_now = WorkerThreadSynchronizeNow::Metric::new();

	#[allow(unused_labels)]
	'main: loop {
		metric_find_work.enter();
		let token = loop {
			server_rust_common::log!("Looking for work");
			// Try popping the queue right away
			match queue.pop_copy() {
				Some(token) => break token,
				None => {
					metric_synchronize_now.enter();
					context.synchronize_now();
					metric_synchronize_now.leave();

					metric_find_work_block.enter();
					#[allow(unused_variables)]
					let is_shutting_down = sync.wait_for_work();
					metric_find_work_block.leave();

					#[cfg(feature = "metrics")]
					if is_shutting_down {
						metric_find_work.leave();
						break 'main;
					}
				}
			}
		};
		server_rust_common::log!("Popped task {}", token);
		metric_find_work.leave();
		
		// SAFETY: Close your eyes
		let task_hold: &'static mut CountTaskHolder = unsafe {
			TaskHolder::from_token(token)
		};

		let mut version = task_hold.update_dequeue();
		'outer: loop {
			// inner loop to loop around synchronize
			loop {
				server_rust_common::log!("Running task {}", token);
				metric_run_task.enter();
				// SAFETY: The task is inprogress
				let result = unsafe { 
					task_hold.data().run(&mut context)?
				};
				metric_run_task.leave();
				server_rust_common::log!("Task {} result {:?}", token, result);
				match result {
					CountTaskResult::Continue => break,
					CountTaskResult::Delete => {
						unsafe { task_hold.update_dead() }
						break 'outer;
					}
					CountTaskResult::Synchronize => {
						metric_synchronize_now.enter();
						context.synchronize_now();
						metric_synchronize_now.leave();

						metric_issue_barrier.enter();
						sync.issue_barrier();
						metric_issue_barrier.leave();
					}
				}
			}

			// SAFETY: at this point the task is inprogress
			version = match unsafe { task_hold.update_free(version) } {
				Ok(()) => break,
				Err(new_version) => new_version
			};
		}
	}

	#[cfg(feature = "metrics")]
	Ok(())
}

fn main() -> std::io::Result<()> {
	let mut poll = Poll::new()?;
	let mut events = Events::with_capacity(128);

	#[cfg(feature = "metrics")]
	{
		use std::os::unix::io::AsRawFd;

		let stdin = std::io::stdin().as_raw_fd();
		let mut fd = mio::unix::SourceFd(&stdin);
		poll.registry().register(
			&mut fd,
			STDIN_TOKEN,
			Interest::READABLE
		)?;
	}

	let listener = {
		let address = server_rust_common::parse_args();

		let mut listener = TcpListener::bind(
			address.to_socket_addrs().unwrap().next().unwrap()
		)?;
		eprintln!("Listening on {:?}", listener.local_addr().unwrap());
		poll.registry().register(&mut listener, SERVER_TOKEN, Interest::READABLE)?;

		listener
	};

	let (mut queue_producer, queue_consumer) = TaskQueue::new_copy(0);
	let worker_handler = LocalSetsHandler::new();
	let worker_sync = WorkerSync::new();

	let workers = {
		let mut workers = Vec::with_capacity(THREAD_COUNT);

		for _ in 0 .. THREAD_COUNT {
			let queue = queue_consumer.clone();
			let context = worker_handler.new_worker_context();
			let sync = worker_sync.clone();

			let worker = std::thread::spawn(
				|| worker_thread_run(queue, context, sync).unwrap()
			);

			workers.push(worker);
		}

		workers
	};

	let result = 'main: loop {
		server_rust_common::log!("Polling");
		match poll.poll(&mut events, None) {
			Ok(()) => (),
			Err(err) => break 'main Err(err)
		};

		for event in events.iter() {
			server_rust_common::log!("Event {:?}", event);
			match event.token() {
				SERVER_TOKEN => {
					loop {
						let (connection, peer_addr) = match listener.accept() {
							Ok(c) => c,
							Err(err) if err.kind() == std::io::ErrorKind::WouldBlock => break,
							Err(err) => break 'main Err(err)
						};

						let leaked_holder: &'static mut CountTaskHolder = TaskHolder::new_leak(
							CountTask::new(poll.registry(), connection)
						);
						let token = Token(leaked_holder as *const CountTaskHolder as usize);

						server_rust_common::log!("Registering {} with address {:?}", token.0, peer_addr);
						match poll.registry().register(
							unsafe { leaked_holder.data().connection() },
							token,
							Interest::READABLE
						) {
							Ok(()) => (),
							Err(err) => break 'main Err(err)
						}
					}
				}
				#[cfg(feature = "metrics")]
				STDIN_TOKEN => {
					let mut buffer = String::new();
					std::io::stdin().read_line(&mut buffer).unwrap();
					if buffer.starts_with("quit") {
						break 'main Ok(());
					}
				}
				Token(token) => {
					let leaked_holder = unsafe {
						TaskHolder::<CountTask>::from_token(token)
					};

					if event.is_error() {
						server_rust_common::log!("Error event {}", token);
					} else if leaked_holder.update_enqueue() {
						server_rust_common::log!("Pushing task {}", token);
						queue_producer.push_copy(token);
						worker_sync.work_cond.notify_one();
					}
				}
			}
		}
	};

	worker_sync.issue_shutdown();
	for worker in workers {
		worker.join().unwrap();
	}

	result
}
