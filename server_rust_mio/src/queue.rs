use std::{cell::UnsafeCell, fmt::Debug, sync::{Arc, atomic::{AtomicUsize, Ordering}}};

pub struct FixedQueue<T, const N: usize> {
	data: UnsafeCell<[T; N]>,
	head: AtomicUsize,
	tail: AtomicUsize,

	#[cfg(debug_assertions)]
	push_count: AtomicUsize,
	#[cfg(debug_assertions)]
	pop_count: AtomicUsize,
}
impl<T: Copy, const N: usize> FixedQueue<T, N> {
	pub fn new_copy(value: T) -> (FixedQueueProducer<T, N>, FixedQueueConsumer<T, N>) {
		Self::new([value; N])
	}
}
impl<T, const N: usize> FixedQueue<T, N> {
	pub fn new(values: [T; N]) -> (FixedQueueProducer<T, N>, FixedQueueConsumer<T, N>) {
		let queue = FixedQueue {
			data: UnsafeCell::new(values),
			head: AtomicUsize::new(0),
			tail: AtomicUsize::new(0),

			#[cfg(debug_assertions)]
			push_count: AtomicUsize::new(0),
			#[cfg(debug_assertions)]
			pop_count: AtomicUsize::new(0),
		};
		let queue = Arc::new(queue);

		let producer = FixedQueueProducer { queue: queue.clone() };
		let consumer = FixedQueueConsumer { queue };

		(producer, consumer)
	}
}
unsafe impl<T, const N: usize> Send for FixedQueue<T, N> {}
unsafe impl<T, const N: usize> Sync for FixedQueue<T, N> {}

pub struct FixedQueueProducer<T, const N: usize> {
	queue: Arc<FixedQueue<T, N>>
}
impl<T: Copy + Debug, const N: usize> FixedQueueProducer<T, N> {
	pub fn push_copy(&mut self, value: T) {
		let head = self.queue.head.load(Ordering::Relaxed);
		let tail = self.queue.tail.load(Ordering::Acquire);
		if (head + 1) % N == tail {
			panic!("Queue full (head: {}, tail: {}, size: {})", head, tail, N);
		}

		// SAFETY: We are the only producer
		unsafe {
			let array = &mut *self.queue.data.get();
			array[head] = value;
		}

		self.queue.head.store(
			(head + 1) % N,
			Ordering::Release
		);

		#[cfg(debug_assertions)]
		{
			let push = self.queue.push_count.fetch_add(1, Ordering::Relaxed);
			let pop = self.queue.pop_count.load(Ordering::Relaxed);
			server_rust_common::log!("push/pop {}/{}", push + 1, pop);
		}
	}
}

#[derive(Clone)]
pub struct FixedQueueConsumer<T, const N: usize> {
	queue: Arc<FixedQueue<T, N>>
}
impl<T: Copy + Debug, const N: usize> FixedQueueConsumer<T, N> {
	pub fn pop_copy(&mut self) -> Option<T> {
		// let lock = self.queue.debug_mutex.lock();

		let mut tail = self.queue.tail.load(Ordering::Relaxed);
		let value = loop {
			let head = self.queue.head.load(Ordering::Acquire);
			if tail == head {
				return None;
			}

			// Read the value before the compare_exchange since it's copy
			// this way we don't race with the producer
			// SAFETY: it's Copy and the memory is initialized
			let value = unsafe {
				let array = &*self.queue.data.get();
				array[tail]
			};

			let new_tail = (tail + 1) % N;
			tail = match self.queue.tail.compare_exchange_weak(tail, new_tail, Ordering::Relaxed, Ordering::Relaxed) {
				Ok(_) => break value,
				Err(tail) => tail
			};
		};

		#[cfg(debug_assertions)]
		{
			let pop = self.queue.pop_count.fetch_add(1, Ordering::Relaxed);
			let push = self.queue.push_count.load(Ordering::Relaxed);
			server_rust_common::log!("pop/push {}/{}", pop + 1, push);
		}

		Some(value)
	}
}

#[cfg(test)]
mod test {
    use super::FixedQueue;

	#[test]
	fn test_fixed_queue() {
		const THREAD_COUNT: usize = 24;
		const TOTAL_COUNT: usize = 100_000;

		let (mut producer, consumer) = FixedQueue::<usize, TOTAL_COUNT>::new_copy(0);

		let threads = (0 .. THREAD_COUNT).map(
			|_| {
				let consumer = consumer.clone();
				let thread = std::thread::spawn(
					move || {
						let mut counter = 0;
						let mut consumer = consumer;
						loop {
							match consumer.pop_copy() {
								None => continue,
								Some(0) => break,
								Some(value) => { counter += value; }
							}
						}

						counter
					}
				);

				thread
			}
		).collect::<Vec<_>>();

		for _ in 0 .. TOTAL_COUNT {
			producer.push_copy(1);
		}
		for _ in 0 .. THREAD_COUNT {
			producer.push_copy(0);
		}


		let count = threads.into_iter().fold(0, |acc, curr| curr.join().unwrap() + acc);

		assert_eq!(count, TOTAL_COUNT);
	}
}