use std::{io::{self, Read, Write}, ptr::NonNull};

use mio::{net::TcpStream};

use server_rust_common::{MESSAGE_SIZE_SIZE, COUNT_MESSAGE_SIZE, LocalSetsWorker, proto::Responder};

// Manual state machine
#[derive(Debug, PartialEq)]
enum CountTaskStage {
	ReadSize,
	ReadMessage(usize),
	PostWords(usize),
	GetCount,
	SendResponse
}
#[derive(Debug)]
pub enum CountTaskResult {
	Continue,
	/// Instructs the runtime to synchronize all other threads and invoke the task again.
	Synchronize,
	Delete
}

pub struct CountTask {
	mio_registry: NonNull<mio::Registry>,
	connection: TcpStream,
	buffer: Vec<u8>,
	responder: Responder,
	stage: CountTaskStage
}
impl CountTask {
	pub fn new(registry: &mio::Registry, connection: TcpStream) -> Self {
		CountTask {
			mio_registry: registry.into(),
			connection,
			buffer: Vec::new(),
			responder: Responder::new(),
			stage: CountTaskStage::ReadSize
		}
	}

	pub fn connection(&mut self) -> &mut TcpStream {
		&mut self.connection
	}

	pub fn run(&mut self, worker_context: &mut LocalSetsWorker) -> io::Result<CountTaskResult> {
		match self.stage {
			CountTaskStage::ReadSize => {
				match self.connection.read_to_end(&mut self.buffer) {
					Ok(read_size) => {
						server_rust_common::log!("Socket read to end in read size: {}", read_size);
						match self.do_read_size(worker_context) {
							Ok(CountTaskResult::Continue) => Ok(CountTaskResult::Delete),
							r => r
						}
					}
					Err(err) if err.kind() == io::ErrorKind::WouldBlock => self.do_read_size(worker_context),
					Err(err) => return Err(err)
				}
			}

			CountTaskStage::ReadMessage(message_size) => {
				match self.connection.read_to_end(&mut self.buffer) {
					Ok(read_size) => {
						server_rust_common::log!("Socket read to end in read message: {}", read_size);
						match self.do_read_message(worker_context, message_size) {
							Ok(CountTaskResult::Continue) => Ok(CountTaskResult::Delete),
							r => r
						}
					}
					Err(err) if err.kind() == io::ErrorKind::WouldBlock => self.do_read_message(worker_context, message_size),
					Err(err) => return Err(err)
				}
			}

			CountTaskStage::PostWords(message_size) => self.do_post_words(worker_context, message_size),
			CountTaskStage::GetCount => self.do_get_count(worker_context),

			CountTaskStage::SendResponse => self.do_send_response(worker_context)
		}
	}

	fn do_read_size(&mut self, worker_context: &mut LocalSetsWorker) -> io::Result<CountTaskResult> {
		debug_assert_eq!(self.stage, CountTaskStage::ReadSize);
		server_rust_common::log!("do_read_size()");
		
		if self.buffer.len() < MESSAGE_SIZE_SIZE {
			server_rust_common::log!("Need more data: {} < {}", self.buffer.len(), MESSAGE_SIZE_SIZE);
			// wait for more data
			return Ok(CountTaskResult::Continue)
		}

		let message_size = u32::from_be_bytes(
			[self.buffer[0], self.buffer[1], self.buffer[2], self.buffer[3]]
		) as usize;

		self.stage = CountTaskStage::ReadMessage(message_size);
		self.do_read_message(worker_context, message_size)
	}

	fn do_read_message(&mut self, worker_context: &mut LocalSetsWorker, message_size: usize) -> io::Result<CountTaskResult> {
		debug_assert_eq!(self.stage, CountTaskStage::ReadMessage(message_size));
		server_rust_common::log!("do_read_message({})", message_size);
		
		if self.buffer.len() < MESSAGE_SIZE_SIZE + message_size {
			server_rust_common::log!("Need more data: {} < {}", self.buffer.len(), MESSAGE_SIZE_SIZE + message_size);
			// wait for more data
			return Ok(CountTaskResult::Continue)
		}

		if message_size == COUNT_MESSAGE_SIZE {
			self.stage = CountTaskStage::GetCount;
			// request synchronization before continuing
			return Ok(CountTaskResult::Synchronize);
		} else {
			self.stage = CountTaskStage::PostWords(message_size);
			self.do_post_words(worker_context, message_size)
		}
	}

	fn do_post_words(&mut self, worker_context: &mut LocalSetsWorker, message_size: usize) -> io::Result<CountTaskResult> {
		debug_assert_eq!(self.stage, CountTaskStage::PostWords(message_size));
		server_rust_common::log!("do_post_words({})", message_size);
		
		let result = worker_context.perform_work(
			&self.buffer[MESSAGE_SIZE_SIZE .. MESSAGE_SIZE_SIZE + message_size]
		);
		match result {
			Ok(()) => (),
			Err(_err) => return Err(io::ErrorKind::InvalidData.into())
		}

		self.responder.post_words_response();

		// get rid of the processed data
		self.buffer.drain(.. MESSAGE_SIZE_SIZE + message_size);

		self.stage = CountTaskStage::SendResponse;
		self.do_send_response(worker_context)
	}

	fn do_get_count(&mut self, worker_context: &mut LocalSetsWorker) -> io::Result<CountTaskResult> {
		debug_assert_eq!(self.stage, CountTaskStage::GetCount);
		server_rust_common::log!("do_get_count()");

		let count = worker_context.end_epoch();

		self.responder.get_count_response(count);

		// get rid of the processed data
		self.buffer.drain(.. MESSAGE_SIZE_SIZE + COUNT_MESSAGE_SIZE);

		self.stage = CountTaskStage::SendResponse;
		self.do_send_response(worker_context)
	}

	fn do_send_response(&mut self, worker_context: &mut LocalSetsWorker) -> io::Result<CountTaskResult> {
		debug_assert_eq!(self.stage, CountTaskStage::SendResponse);
		server_rust_common::log!("do_send_response()");
		
		// TODO: Is this sufficient?
		loop {
			server_rust_common::log!("response loop");
			match self.connection.write_all(self.responder.buffer()) {
				Ok(()) => break,
				Err(err) if err.kind() == io::ErrorKind::WouldBlock => (),
				Err(err) => return Err(err)
			}
		}
		server_rust_common::log!("Response sent");

		self.stage = CountTaskStage::ReadSize;
		// attempt another message right away
		self.do_read_size(worker_context)
	}
}
impl Drop for CountTask {
	fn drop(&mut self) {
		server_rust_common::log!("Dropping task {}", self as *const _ as usize);

		unsafe {
			self.mio_registry.as_ref().deregister(&mut self.connection).unwrap();
		}
	}
}