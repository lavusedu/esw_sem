#!/bin/sh

git pull

RUSTFLAGS='-C target-cpu=native' cargo build --release
strip target/release/server_rust_mio

if [ "$1" = "ritchie" ]; then
	ESW_HOST=ritchie.ciirc.cvut.cz ESW_PORT=8633 ./target/release/server_rust_mio
fi