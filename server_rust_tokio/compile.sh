#!/bin/sh

RUSTFLAGS='-C target-cpu=native' cargo build --release
strip target/release/server_rust_tokio