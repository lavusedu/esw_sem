use std::sync::Arc;

use tokio::{io::{AsyncReadExt, AsyncWriteExt}, net::{TcpListener, TcpStream}, task::JoinHandle};

use server_rust_common::{named_metric, Mutex, COUNT_MESSAGE_SIZE, SharedSetsHandler, SharedSetsWorker, proto::Responder, metrics::Metrics};

named_metric!(ConnectionPostWords);
named_metric!(ConnectionGetCount);
named_metric!(ConnectionContextEndEpoch);

#[derive(Clone)]
struct ConnectionContext {
	handler: SharedSetsHandler,
	epoch_tasks: Arc<Mutex<Vec<JoinHandle<()>>>>,
	metrics_end_epoch: ConnectionContextEndEpoch::Metric
}
impl ConnectionContext {
	pub fn new() -> Self {
		ConnectionContext {
			handler: SharedSetsHandler::new(),
			epoch_tasks: Arc::new(
				Mutex::new(
					Vec::new()
				)
			),
			metrics_end_epoch: Metrics::new()
		}
	}

	pub fn new_worker(&mut self) -> SharedSetsWorker {
		self.handler.new_worker()
	}

	pub fn register_task(&mut self, handle: JoinHandle<()>) {
		let mut lock = self.epoch_tasks.lock();

		lock.push(handle);
	}

	pub async fn end_epoch(&mut self) -> i32 {
		self.metrics_end_epoch.enter();

		let mut tasks = {
			let mut lock = self.epoch_tasks.lock();

			std::mem::replace(&mut *lock, Vec::new())
		};

		for task in tasks.drain(..) {
			task.await.unwrap();
		}

		let result = self.handler.end_epoch();
		self.metrics_end_epoch.leave();

		result
	}
}

async fn handle_connection(
	mut stream: TcpStream,
	mut context: ConnectionContext
) -> tokio::io::Result<()> {
	let mut responder = Responder::new();
	let mut throwaway_buffer = [0u8; COUNT_MESSAGE_SIZE];

	let mut post_metrics = ConnectionPostWords::Metric::new();
	let mut get_metrics = ConnectionGetCount::Metric::new();

	server_rust_common::log!("Connection opened: {:?}", stream.peer_addr());
	loop {
		// whenever a new request is detected the current epoch is determined by the current value of the 
		let message_size = match stream.read_u32().await {
			Ok(s) => s,
			Err(err) if err.kind() == std::io::ErrorKind::UnexpectedEof => break,
			Err(err) => return Err(err)
		};
		server_rust_common::log!("recv({})", message_size);

		// hax - words message cannot be this short even if it's empty because it's compressed
		if message_size as usize == COUNT_MESSAGE_SIZE {
			get_metrics.enter();

			stream.read_exact(&mut throwaway_buffer).await?;

			// GET count
			let counter = context.end_epoch().await;
			responder.get_count_response(counter);
			stream.write_all(responder.buffer()).await?;

			get_metrics.leave();
		} else {
			post_metrics.enter();

			// POST words
			let mut worker = context.new_worker();

			// read the message in normal tokio runtime
			worker.buffer_mut().resize(message_size as usize, 0);
			stream.read_exact(worker.buffer_mut()).await?;

			let task = tokio::task::spawn(async move {
				worker.perform_work().unwrap();
			});
			context.register_task(task);
			
			responder.post_words_response();
			stream.write_all(responder.buffer()).await?;


			post_metrics.leave();
		}
	}
	server_rust_common::log!("Connection closed: {:?}", stream.peer_addr());

	Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
	let address = server_rust_common::parse_args();
	
	let listener = TcpListener::bind(address).await?;
	eprintln!("Listening on {:?}", listener.local_addr().unwrap());

	let connection_context = ConnectionContext::new();

	loop {
		let (stream, _) = listener.accept().await?;

		tokio::task::spawn(
			handle_connection(
				stream,
				connection_context.clone()
			)
		);
	}

	// Ok(())
}
