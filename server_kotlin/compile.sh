#!/bin/sh

git pull

gradle :jar

if [ "$1" = "ritchie" ]; then
	java -jar build/libs/server_kotlin-1.0-SNAPSHOT.jar ritchie.ciirc.cvut.cz 8633
fi