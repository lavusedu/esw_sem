package cz.cvut.fel.esw.server

import com.google.protobuf.CodedOutputStream
import cz.cvut.fel.esw.server.proto.Response
import io.ktor.utils.io.*
import kotlinx.coroutines.yield
import java.util.zip.Inflater

private class VarintDecoder {
	private var result: Int = 0
	private var shift: Int = 0

	fun reset() {
		this.result = 0
		this.shift = 0
	}

	fun process(byte: Byte): Int? {
		val value = byte.toUByte().toInt()

		this.result = this.result.or(
			value.and(0x7F).shl(this.shift * 7)
		)
		this.shift += 1

		if (value.and(0x80) == 0) {
			val result = this.result
			this.reset()

			return result
		}

		return null
	}
}

private suspend fun readWordsMessageSize(
	reader: ByteReadChannel
): Int {
	val decoder = VarintDecoder()

	// request tag
	var decoded = decoder.process(reader.readByte())
	while (decoded == null) {
		decoded = decoder.process(
			reader.readByte()
		)
	}
	assert(decoded == 18) // 2 << 3 | 2

	// ??
	decoded = decoder.process(reader.readByte())
	while (decoded == null) {
		decoded = decoder.process(
			reader.readByte()
		)
	}

	// PostWords
	decoded = decoder.process(reader.readByte())
	while (decoded == null) {
		decoded = decoder.process(
			reader.readByte()
		)
	}
	assert(decoded == 10) // 2 << 3 | 2

	// data len
	decoded = decoder.process(reader.readByte())
	while (decoded == null) {
		decoded = decoder.process(
			reader.readByte()
		)
	}

	// kotlin smort, kotlin see decoded is non-null by this point
	return decoded
}

private suspend fun writeResponse(
	writer: ByteWriteChannel,
	response: Response
) {
	val messageSize = response.serializedSize

	val buffer = ByteArray(16)

	buffer[3] = messageSize.and(0xFF).toByte()
	buffer[2] = messageSize.shr(8).and(0xFF).toByte()
	buffer[1] = messageSize.shr(16).and(0xFF).toByte()
	buffer[0] = messageSize.shr(24).and(0xFF).toByte()

	val stream = CodedOutputStream.newInstance(
		buffer, 4, buffer.size - 4
	)

	// we can suppress this because we know this doesn't actually do any IO
	@Suppress("BlockingMethodInNonBlockingContext")
	response.writeTo(stream)

	writer.writeFully(buffer, 0, messageSize + 4)
}

private suspend fun runGetCount(
	writer: ByteWriteChannel,
	localSet: HashSet<Word>
) {
	updateGlobalSet(localSet)

	// wait until we are the only working thread
	while (workingThreads.get() > 1) {
		yield()
	}

	globalSet[Word()] = Unit

	val count = globalSet.size - 1
	globalSet.clear()

	log("[${Thread.currentThread().id}] runGetCount(${count})")

	val response = Response.newBuilder().setCounter(count).build()
	writeResponse(writer, response)
}

private fun processWord(
	buffer: ByteArray,
	start: Int,
	end: Int,
	localSet: HashSet<Word>
) {
	val word = buffer.sliceArray(start until end)
	localSet.add(Word(word))
}

private suspend fun runCountWords(
	inflater: Inflater,
	writer: ByteWriteChannel,
	localSet: HashSet<Word>
) {
	log("[${Thread.currentThread().id}] runCountWords")

	val buffer = ByteArray(1024)
	var currentStart = 0
	while (true) {
		val inflatedBytes = inflater.inflate(buffer, currentStart, buffer.size - currentStart) + currentStart
//		log("Current buffer: ${String(buffer, 0, inflatedBytes)}")

		var start = 0
		for (current in currentStart until inflatedBytes) {
			val byte = buffer[current].toUByte().toInt()
			if (
				byte == ' '.code
				|| byte == '\n'.code
				|| byte == '\t'.code
				|| byte == '\r'.code
			) {
				processWord(
					buffer,
					start,
					current,
					localSet
				)

				start = current + 1
			}
		}

		if (inflater.finished()) {
			// don't forget the last word
			if (start < inflatedBytes) {
				processWord(
					buffer,
					start,
					inflatedBytes,
					localSet
				)
			}

			break
		}

		// move the remaining part to the start of the buffer
		currentStart = inflatedBytes - start
		for (current in 0 until currentStart) {
			buffer[current] = buffer[start + current]
		}
	}
	inflater.end()

	val response = Response.getDefaultInstance()
	writeResponse(writer, response)
}

suspend fun countTask(
	reader: ByteReadChannel,
	writer: ByteWriteChannel,
	localSet: HashSet<Word>
) {
	// other suspend functions are automatically awaited
	val messageSize = reader.readInt()
	if (messageSize == 2) {
		reader.discard(2)
		return runGetCount(writer, localSet)
	}

	val wordsSize = readWordsMessageSize(reader)
	val wordsBuffer = ByteArray(wordsSize)
	reader.readFully(wordsBuffer)

	val inflater = Inflater(true)
	inflater.setInput(wordsBuffer, 10, wordsBuffer.size - 10)

	runCountWords(inflater, writer, localSet)
}