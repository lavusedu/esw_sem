package cz.cvut.fel.esw.server

class Word(
	private val inner: ByteArray
) {
	constructor() : this(ByteArray(0))

	override fun equals(other: Any?): Boolean {
		if (this.javaClass != other?.javaClass) {
			return false
		}
		other as Word

		if (this.inner.size != other.inner.size) {
			return false
		}

		for (i in this.inner.indices) {
			if (this.inner[i] != other.inner[i]) {
				return false
			}
		}

		return true
	}

	override fun hashCode(): Int {
		return this.innerHashCode()
	}

	private fun innerHashCode(): Int {
		var hash = inner.size
		for (element in inner) {
			hash = hash * 31 + element.hashCode()
		}

		return hash
	}
}