package cz.cvut.fel.esw.server

import kotlinx.coroutines.Job
import java.util.concurrent.atomic.AtomicInteger

class AtomicFixedJobQueue(size: Int) {
	private val tasks = Array<Job?>(size) { null }
	private val head = AtomicInteger(0)
	private val tail = AtomicInteger(0)

	fun push(value: Job) {
		val head = this.head.getPlain()
		this.tasks[head] = value

		val newHead = (head + 1) % this.tasks.size
		this.head.compareAndExchangeRelease(head, newHead)

		log("[${Thread.currentThread().id}] pushed to queue: $head")
	}

	fun pop(): Job? {
		var tail = this.tail.get()
		var job: Job?

		while (true) {
			val head = this.head.getAcquire()
			if (head == tail) {
				return null
			}

			job = this.tasks[tail]
			val newTail = (tail + 1) % this.tasks.size

			val witnessed = this.tail.compareAndExchange(tail, newTail)
			if (witnessed == tail) {
				log("[${Thread.currentThread().id}] popped from queue: $tail (${job})")
				break
			} else {
				tail = witnessed
			}
		}

		return job!!
	}
}