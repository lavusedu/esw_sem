package cz.cvut.fel.esw.server

import io.ktor.network.selector.*
import io.ktor.network.sockets.*
import kotlinx.coroutines.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

fun log(v: Any?) {
	if (false) {
		System.err.println(v)
	}
}

fun main(args: Array<String>) {
	// gets the first element or defaults to 127.0.0.1
	val hostname = args.getOrElse(0) { "127.0.0.1" }
	// gets the second element or null
	// if an element is returned calls .toInt() on it
	// otherwise defaults to 8080
	val port = args.getOrNull(1)?.toInt() ?: 8080

	runBlocking {
		// nio selector
		val selector = ActorSelectorManager(Dispatchers.IO)
		val socket = aSocket(selector).tcp().bind(
			hostname = hostname,
			port = port
		)
		println("Listening on ${hostname}:${port}")

		while (true) {
			val connection = socket.accept()

			// spawns a new coroutine (async task) on the current executor
			launch {
				handleConnection(connection)
			}
		}
	}
}

val globalSet: ConcurrentHashMap<Word, Unit> = ConcurrentHashMap<Word, Unit>(100_000)
val workingThreads: AtomicInteger = AtomicInteger(0)

fun updateGlobalSet(
	localSet: HashSet<Word>
) {
	for (word in localSet) {
		globalSet[word] = Unit
	}
	localSet.clear()
}

suspend fun handleConnection(
	connection: Socket
) {
	val readChannel = connection.openReadChannel()
	val writeChannel = connection.openWriteChannel(true)
	val localSet = HashSet<Word>(250_000)

	while (!readChannel.isClosedForRead) {
		readChannel.awaitContent()
		workingThreads.incrementAndGet()

		val job = coroutineScope {
			// select executor to run on the thread pool
			async(Dispatchers.Default) {
				while (readChannel.availableForRead >= 4) {
					countTask(readChannel, writeChannel, localSet)
				}
				updateGlobalSet(localSet)
			}
		}
		job.join()

		workingThreads.decrementAndGet()
	}
}