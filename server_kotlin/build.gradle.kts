import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.0"
    application
}

group = "cz.cvut.fel.esw.server"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.ktor:ktor-network:1.5.4")
    implementation("com.google.protobuf:protobuf-java:3.16.0")
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "15"
}

application {
    mainClass.set("cz.cvut.fel.esw.server.MainKt")
}

tasks.jar {
    manifest {
        attributes["Main-Class"] = "cz.cvut.fel.esw.server.MainKt"
    }

    duplicatesStrategy = DuplicatesStrategy.INCLUDE

    from(sourceSets.main.get().output)
    dependsOn(configurations.runtimeClasspath)
    from({
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
    })
}