# ESW Sem Protobuf servers

This project is available at https://gitlab.fel.cvut.cz/lavusedu/esw_sem.

## Reference

The `reference` folder contains a reference python implementation of the server and a simple tester program.

## Rust

To run the Rust mio-based server, navigate to the `server_rust_mio` folder and run `./compile.sh`. This simple script runs Cargo to build the server in release mode with cpu=native compiler flag and strips the binary. To run the final product on ritchie, invoke:

```sh
ESW_HOST=ritchie.ciirc.cvut.cz ESW_PORT=8633 ./target/release/server_rust_mio
```

or change to your own host and port values. When the env variables are not present, the default host value is `127.0.0.1` while the default port is `8080`.

## Kotlin

Kotlin implementation uses Ktor framework with raw sockets. The tasks are generated on the main thread and then distributed over a threadpool. To run the Kotlin server, navigate to the `server_kotlin` folder and run `./compile.sh`. This simple script runs gradle to build a jar file. To run the final product on ritchie, invoke:

```sh
java -jar build/libs/server_kotlin-1.0-SNAPSHOT.jar ritchie.ciirc.cvut.cz 8633
```

or chang eto your own host and port values. The default arguments are `127.0.0.1` and `8080`.
